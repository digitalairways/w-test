(function() {
    var factory = function (gibson) {


        gibson.export.tokenizer = function(token) {
            var regexs = [
                /^checkbox: *(\n|.+)/
            ];
            return WidgetBuilder.tokenizer(regexs, token);
        };

        gibson.export.token = function(name, res, src) {
            var token = { type: "widget", name: name };
            var data = {};
            var params = extractParams(res[1]);
            data = {
              inputLabel: params[0],
              inputGpfId: params[1]
            }
            token.data = data;

            return token;
        };
        function extractParams(t) {
            var params = [];
                params = t.split(",");
            return params.map(function(el){ return el.trim(); });
        }
    };

    factory(window.gibson);
})();
