(function() {

    var factory = function (gibson) {

        var widgetName = "w-checkbox";

        gibson.export.renderer = {

            html: function(token) {
              var INSTANCE_ID = Gibson.generateWidgetID();
              gibson.widgets[widgetName].tokens[INSTANCE_ID] = token;

              var html = $("<div>", {
                "widget-id": INSTANCE_ID
              });

              var i = $("<input>", {
                type: 'checkbox',
                id: token.data.inputGpfId
              });
              var l = $("<label>", {
                for: token.data.inputGpfId,
                text: token.data.inputLabel,
		class: "w-chckbx-sz"
              });

              if(token.data.inputGpfId){
                Gibson.persistWidgetField(i, token.data.inputGpfId);
              }

              html.append(i).append(l);
              return html[0].outerHTML;
        }
      };

        gibson.export.listeners = function() {
        };

    };

    factory(window.gibson)

})();
